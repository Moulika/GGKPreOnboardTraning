﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    class Node
    {
        public int item;
        public Node left;
        public Node right;
    }
    class BSTTree
    {
        public Node root;
        public List<int> Elements = new List<int>();
        public BSTTree()
        {
            root = null;
        }
        public Node ReturnRoot()
        {
            return root;
        }
        public void Search(int numberGiven)
        {
            Node present = root;
            while (true)
            {
                if (numberGiven < present.item)
                {
                    present = present.left;
                    if (present == null)
                    {
                        Console.WriteLine("Element no found");
                        return;
                    }
                }
                else if (numberGiven > present.item)
                {
                    present = present.right;
                    if (present == null)
                    {
                        Console.WriteLine("Element not found");
                        return;
                    }

                }
                else if (numberGiven == present.item)
                {
                    Console.WriteLine("Element Found");
                    break;
                }

            }
        }
        public void Insert(int value)
        {
            Node newNode = new Node();
            newNode.item = value;
            if (root == null)
            {
                root = newNode;
            }
            else
            {
                Node current = root;
                Node parent;
                while (true)
                {
                    parent = current;
                    if (value < current.item)
                    {
                        current = current.left;
                        if (current == null)
                        {
                            parent.left = newNode;
                            return;
                        }
                    }
                    else
                    {
                        current = current.right;
                        if (current == null)
                        {
                            parent.right = newNode;
                            return;
                        }
                    }
                }
            }
        }
        public void Inorder(Node Root)
        {
            if (Root != null)
            {
                Inorder(Root.left);
                Elements.Add(Root.item);
                Inorder(Root.right);
            }
        }
        public void Descending()
        {
            Inorder(ReturnRoot());
            Elements.Reverse();
            foreach (int i in Elements)
            {
                Console.WriteLine(i);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Tree theTree = new Tree();
            int number, numberGiven, choice;
            Console.WriteLine("Enter numbers and -1 to break");
            while (true)
            {
                number = Int32.Parse(Console.ReadLine());
                if (number == -1)
                    break;
                else
                {
                    theTree.Insert(number);
                }
            }

            Console.WriteLine("Enter your choice");
            Console.WriteLine("1.Search an Element");
            Console.WriteLine("2.Ascending Order");
            Console.WriteLine("3.Descending Order");
            choice = Int32.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.Write("Enter the no.to search!☺");
                    numberGiven = Int32.Parse(Console.ReadLine());
                    theTree.Search(numberGiven);
                    break;

                case 2:
                    theTree.Inorder(theTree.ReturnRoot());
                    foreach (int i in theTree.Elements)
                    {
                        Console.WriteLine(i);
                    }
                    break;
                case 3:
                    theTree.Descending();
                    break;


            }
        }
    }
}
