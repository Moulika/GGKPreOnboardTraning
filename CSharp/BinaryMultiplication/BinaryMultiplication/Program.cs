﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
namespace BinaryMultiplication
{
    class Program
    {
        List<int> integerToBinary(int integral,List<int> toBinary)
        {
            while(integral!=0)
            {
                
                toBinary.Add(integral % 2);
                integral= integral / 2;
            }
            toBinary.Reverse();
            return toBinary;

        }
        List<int> fractionToBinary(double fraction,List<int> toBinary)
        {
            fraction*= 2;
            int precisionCount = 0;

            while(fraction!=1)
            {
                if (precisionCount >= 10)
                    break;

                if (fraction>1)
                {
                    toBinary.Add(1);
                    fraction = fraction - 1;
                }
                else
                {
                    toBinary.Add(0);
                }
                fraction *= 2;
                ++precisionCount;
            }
            if (fraction == 1)
                toBinary.Add(1);
            return toBinary;
        }
        public BigInteger binaryproduct(BigInteger binary1, BigInteger binary2)
        {
            int i = 0;
            BigInteger remainder = 0,binaryprod=0;
            BigInteger[] sum = new BigInteger[10000];
            
            while (binary1 != 0 || binary2 != 0)
            {
                sum[i++] = (binary1 % 10 + binary2 % 10 + remainder) % 2;
                remainder = (binary1 % 10 + binary2 % 10 + remainder) / 2;
                binary1 = binary1 / 10;
                binary2 = binary2 / 10;
            }
            if (remainder != 0)
                sum[i++] = remainder;
            --i;
            while (i >= 0)
                binaryprod = binaryprod * 10 + sum[i--];
            return binaryprod;
        }
        static void Main(string[] args)
        {

            Program obj = new Program();
            BigInteger binary1 = 0, binary2 = 0;
            double num1, num2;
            int integral1, integral2, fractionCount1, fractionCount2,totalCount=0, count = 0;
            BigInteger sum = 0, multiply;
            double power;
            double fraction1, fraction2;
            List<int> integralList1 = new List<int>();
            List<int> integralList2 = new List<int>();
            List<int> fractionList3 = new List<int>();
            List<int> fractionList4 = new List<int>();
            Console.WriteLine("Enter the float numbers");
            num1 = Double.Parse(Console.ReadLine());
            num2 = Double.Parse(Console.ReadLine());
            integral1 = Convert.ToInt32(Math.Floor(num1));
            integral2 = Convert.ToInt32(Math.Floor(num2));
            fraction1 = num1 - integral1;
            fraction2 = num2 - integral2;
            integralList1 = obj.integerToBinary(integral1, integralList1);
            integralList2 = obj.integerToBinary(integral2, integralList2);
            fractionList3 = obj.fractionToBinary(fraction1, fractionList3);
            fractionList4 = obj.fractionToBinary(fraction2, fractionList4);
            fractionCount1 = fractionList3.Count();
            fractionCount2 = fractionList4.Count();
            totalCount = fractionCount1 + fractionCount2;
            integralList1.AddRange(fractionList3);
            integralList2.AddRange(fractionList4);
            foreach (int i in integralList1)
            {
                binary1 = 10 * binary1 + i;
            }
            foreach (int i in integralList2)
            {
                binary2 = 10 * binary2 + i;
            }
            while (binary2 != 0)
            {
                if (count == 0)
                {
                    sum = binary1 * (binary2 % 10);
                    binary2 = binary2 / 10;
                    count++;
                }
                else
                {
                    power = Math.Pow(10, count);
                    multiply = (binary1 * (binary2 % 10)) * (BigInteger)power;
                    binary2 = binary2 / 10;
                    sum = obj.binaryproduct(sum, multiply);
                    count++;

                }
            }
            double decimalSum = 0,fractionSum=0;
            List<BigInteger> binaryList = new List<BigInteger>();
            while (sum != 0)
            {
                binaryList.Add(sum % 10);
                sum = sum / 10;
            }
            binaryList.Reverse();
            int binCount= binaryList.Count();
            for (int i = binCount-totalCount-1, j = 0; i >=0; i--, j++)
            {
                if(binaryList[i]==1)
                     decimalSum = decimalSum + Math.Pow((int)2, j);
            }
            for(int i=binCount-totalCount,j=-1;i<binCount ;i++,j--)
            {
                if(binaryList[i]==1)
                {
                    fractionSum = fractionSum + Math.Pow((int)2, j);
                }
            }
            double finalSum = fractionSum + decimalSum;
            Console.WriteLine("the multiplication of float numbers is" + finalSum);
        }
    }
}
