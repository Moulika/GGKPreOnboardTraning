﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBST
{
    //create a node
    public class Node<T>
    {
        public T data;
        public Node<T> leftChild;
        public Node<T> rightChild;
        public Node(T data)
        {
            this.data = data;
            this.leftChild = null;
            this.rightChild = null;
        }
    }
    //create a BST
    public class BST<T> where T : IComparable<T>
    {
        public Node<T> root = null;
        private bool left;
        public void add(T n)
        {
            if (this.root == null)
            {
                root = new Node<T>(n);
            }
            else
            {
                Node<T> temp = traverse(n);
                if (!temp.data.Equals(n))
                {
                    if (left)
                        temp.leftChild = new Node<T>(n);
                    else
                        temp.rightChild = new Node<T>(n);
                }
            }
        }
        //search or insertion
        private Node<T> traverse(T n)
        {
            Node<T> temp = this.root;
            while (temp != null)
            {
                if (n.CompareTo(temp.data) < 0)
                {
                    if (temp.leftChild == null)
                    {
                        this.left = true;
                        break;
                    }
                    temp = temp.leftChild;
                }
                else if (n.CompareTo(temp.data) > 0)
                {
                    if (temp.rightChild == null)
                    {
                        this.left = false;
                        break;
                    }
                    temp = temp.rightChild;
                }
                if (n.CompareTo(temp.data) == 0)
                {
                    return temp;
                }
            }
            return temp;
        }
        //confirms whether there's a match or not
        public bool search(T n)
        {
            return this.traverse(n).data.Equals(n);
        }

    }
    class Program
    {
        public static void Main(string[] args)
        {
            BST<int> tree = new BST<int>();
            BSTTree(tree);
            Console.ReadKey();
        }

        private static void BSTTree(BST<int> tree)
        {
            int i, n;
            Console.WriteLine("Enter the number of nodes");
            n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the tree nodes");
            for (i = 0; i < n; i++)
            {
                tree.add(Convert.ToInt32(Console.ReadLine()));
            }
           Console.WriteLine("Enter the node to be searched");
            if (tree.search(Convert.ToInt32(Console.ReadLine())))
            {
                Console.WriteLine("node was found.");
            }
            else
            {
                Console.WriteLine("node not found");
            }
        }
    }
}