﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciPrimePattern
{
    class Program
    { 
            void printPattern(List<int> Fib, int count)
            {
                for (int k = 1; k <= count; k++)
                {
                    for (int l = 0; l < k; l++)
                    {
                        Console.Write(Fib[l]);
                        Console.Write(" ");
                    }
                    Console.WriteLine();
                }
            }
            Boolean checkPrime(int temp)
            {
                int i, j = temp;
                if (j == 1)
                    return false;
                for (i = 2; i < j; i++)
                {
                    if (j % i == 0)
                    {
                        return false;
                    }
                    if (i == j)
                    {
                        return true;
                    }
                }
                return true;
            }
            static void Main(string[] args)
            {
                Program obj = new Program();
                int num, i;
                Console.Write("read input");
                num = Convert.ToInt32(Console.ReadLine());
                List<int> Fib = new List<int>();
                int a = 0, b = 1, count = 0;
                for (i = 0; i < num; i++)
                {
                    int temp = a + b;
                    a = b;
                    b = temp;
                    if (obj.checkPrime(temp))
                    {
                        Fib.Add(temp);
                        count++;
                    }
                }
                obj.printPattern(Fib, count);

            }
        }
    }
