﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDesignPattern
{
    public abstract class IVehicle
    {
      public abstract void build();
        
    }
    class Bus : IVehicle
    {
        public override void build()
        {
            Console.WriteLine("Bus");
        }
    }

    class Car : IVehicle
    {
        public override void build()
        {
            Console.WriteLine("Car");
        }
    }
    class Truck : IVehicle
    {
        public override void build()
        {
            Console.WriteLine("Truck");
        }
    }
    class Factory
    {
        public IVehicle buildVehicle(String input)
        {
            switch(input)
            {
                case "bus":return new Bus();
                case "car":return new Car();
                case "truck":return new Truck();
                default: throw new ArgumentException("Invalid type", "type");
            }
        }
        static void Main(string[] args)
        {
            String input;
            Console.WriteLine("Enter the name of the vehicle");
            input = Console.ReadLine();
            Factory obj = new Factory();
            IVehicle ret=obj.buildVehicle(input);
            ret.build();
        }
    }
}
