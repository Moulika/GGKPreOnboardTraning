﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueUsingGenerics
{
    class Queue<T>
    {
        private int size;
        private T[] queueAr;
        private int left; //left of queue
        private int right;// right of queue
        public Queue(int size)
        {
            this.size = size;
            queueAr = new T[size];
            left = -1;
            right = -1;
        }
        public void insert(T value)
        {
            if(isFull())
            {
                throw new Exception();
            }
            queueAr[++right] = value;
        }
        public Boolean isFull()
        {
            return (right == size - 1);
        }
        public T remove()
        {
            if(isEmpty())
            {
                throw new Exception();
            }
            return queueAr[++left];
        }
        public Boolean isEmpty()
        {
            if(right==-1&&left==-1)
            {
                return true;
            }
            else if(left<right)
            {
                return false;
            }
            return true;

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Queue<int> queue = new Queue<int>(10);
            Console.WriteLine("enter the elements to insert");
            while (true)
            {
                int element;
                element = Convert.ToInt32(Console.ReadLine());
                if(element==-1)
                {
                    break;
                }
                try
                {
                    queue.insert(element);
                }
                catch(Exception e)
                {
                    Console.WriteLine("queue is full");
                    break;
                }
            }
            string remove;
            while(true)
            {
                Console.WriteLine("enter y to remove and no to stop");
                remove = Console.ReadLine();
                if (remove=="n")
                {
                    Console.WriteLine("Deletion is stopped");
                    break;
                }
                else
                {
                    try
                    {
                        Console.WriteLine(queue.remove() + " ");
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine("cannot remove from the empty stack");
                        break;
                    }
                }
            }
        }
    }
}
