﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackUsingGenerics
{
    class Stack<T>
    {
        private int size;
        private T[] stackAr;
        private int top; // top of stack

        public Stack(int size)
        {
            this.size = size;
            stackAr = new T[size]; //Creation of Generic Stack Array
            top = -1; // initialize Stack to with -1
        }
        public void push(T value)
        {
            if (isFull())
            {
                throw new Exception();
            }
            stackAr[++top] = value;
        }
        public T pop()
        {
            if (isEmpty())
            {
                Console.WriteLine("Stack is Empty");
                Environment.Exit(0);
            }
            return stackAr[top--]; // remove item and decrement top as well.
        }
        public Boolean isEmpty()
        {
            return (top == -1);
        }
        public Boolean isFull()
        {
            return (top == size - 1);
        }

    }

    public class StackExampleGeneric
    {
        public static void Main(String[] args)
        {
            Stack<int> stack = new Stack<int>(10); // Creation of Generic Stack
            while (true)
            {
                int element;
                element =Convert.ToInt32( Console.ReadLine());
                if (element == -1)
                {
                    break;
                }
                else
                {
                    try
                    {
                        stack.push(element);
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine("couldnt push into stack");
                        break;
                    }
                }
            }

            Console.WriteLine("Popped items: ");
            while(true)
            {
               string element;
                Console.WriteLine("enter 'y' to pop and 'n' to stop");
                element = Console.ReadLine();
                if (element == "n")
                {
                    break;
                }
                else
                {
                    Console.WriteLine(stack.pop() + " ");
                }
            }
        }

    }
}


