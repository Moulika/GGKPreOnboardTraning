#python program to check whether the given input year is a leap year or not.
try:
    inputYear=int(input("Enter the year to check whether it is LEAP YEAR or not: "))

    if(inputYear % 4 == 0):
        print("\n\nThe given year ",inputYear," is a LEAP YEAR\n")
    else:
        print("\n\nGiven year is NOT a LEAPYEAR\n\n")
except ValueError:
    print("\n\nYear should be an INTEGER only\n\n")

