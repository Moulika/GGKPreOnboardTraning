#python program that takes two numbers as input and swaps them and displays them.
inp1=input("Enter the first number: ")
inp2=input("Enter the second number: ")
print("\nThe numbers before swapping are:")
print("First number: ", inp1)
print("Second number: ", inp2)

temp = inp1
inp1 = inp2
inp2 = temp

print("\n\nThe numbers after swapping are:")
print("First number: ", inp1)
print("Second number: ", inp2)