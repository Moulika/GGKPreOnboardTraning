#Making a claculator

try:
    inp1 = int(input("Enter first number: "))
    inp2 = int(input("Enter second number: "))
    i=1
    while(i>0):
        print("SELECT AN OPERATION FROM BELOW")
        print("1: ADDITION\n2:SUBTRACTION\n3:MULTIPLICATION\n4:DIVISION\n5:QUOTIENT AND REMAINDER\n6:EXIT\n\n")
        choice=int(input("Enter the choice: "))

        if(choice==1):
            print("Addition is: ",(inp1+inp2))
            print("\n\n")
        elif(choice==2):
            print("Subtraction is: ",(inp1-inp2))
            print("\n\n")
        elif(choice==3):
            print("Multiplication is: ",(inp1*inp2))
            print("\n\n")
        elif(choice==4):
            print("Division is: ",(inp1/inp2))
            print("\n\n")
        elif(choice==5):
            print("The Quotient and Remainder are: ",(inp1//inp2),(inp1%inp2))
            print("\n\n")
        else:
            exit()

except ValueError:
    print("\n\nInput is not a number\n\n")