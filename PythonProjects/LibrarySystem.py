bkList=[]

class Book:
    def __init__(self,id,name,author,dept):
        self.bid=id
        self.bname=name
        self.bauth=author
        self.bdept=dept
    def Add(self):
        bookList={}
        bookList['id']=self.bid
        bookList['name']=self.bname
        bookList['author']=self.bauth
        bookList['department']=self.bdept
        bkList.append(bookList)
    def Display():
        for item in bkList:
            print(item)
            print("\n")
    def Delete(obj):
        for i in range(len(bkList)):
            if bkList[i]['id']==obj:
                del bkList[i]
                break
    def Search(obj):
        for i in range(len(bkList)):
            if bkList[i]['id']==obj:
                print("Book found")
                print(bkList[i])
                break
    def Update(id):
        for i in range(len(bkList)):
            if (bkList[i]['id']==id):
                bkList[i]['name']=input("enter updated name: ")
                bkList[i]['author']=input("Enter the updated author: ")
                print("\nUpdate done successfully\n")
                break

            
class Author:
    def __init__(self,id,author,dept):
        self.bid=id
        self.bauth=author
        self.bdept=dept
    def Add(self):
        bookList={}
        bookList['id']=self.bid
        bookList['author']=self.bauth
        bookList['department']=self.bdept
        bkList.append(bookList)
    def Display():
        for item in bkList:
            print(item)
            print("\n")
    def Delete(obj):
        for i in range(len(bkList)):
            if bkList[i]['id']==obj:
                del bkList[i]
                break
    def Search(obj):
        for i in range(len(bkList)):
            if bkList[i]['id']==obj:
                print("Auhtor found")
                print(bkList[i])
                break
    def Update(id):
        for i in range(len(bkList)):
            if (bkList[i]['id']==id):
                bkList[i]['department']=input("enter updated department: ")
                bkList[i]['author']=input("Enter the updated author: ")
                print("\nUpdate done successfully\n")
                break



class Reader:
    def __init__(self,id,reader,dept):
        self.bid=id
        self.bauth=reader
        self.bdept=dept
    def Add(self):
        bookList={}
        bookList['id']=self.bid
        bookList['reader']=self.bauth
        bookList['department']=self.bdept
        bkList.append(bookList)
    def Display():
        for item in bkList:
            print(item)
            print("\n")
    def Delete(obj):
        for i in range(len(bkList)):
            if bkList[i]['id']==obj:
                del bkList[i]
                break
    def Search(obj):
        for i in range(len(bkList)):
            if bkList[i]['id']==obj:
                print("Reader found")
                print(bkList[i])
                break
    def Update(id):
        for i in range(len(bkList)):
            if (bkList[i]['id']==id):
                bkList[i]['department']=input("enter updated department: ")
                bkList[i]['reader']=input("Enter the updated reader: ")
                print("\nUpdate of reader done successfully\n")
                break


class LibraryStaff:
    def __init__(self,id,staff,dept):
        self.bid=id
        self.bauth=reader
        self.bdept=dept
    def Add(self):
        bookList={}
        bookList['id']=self.bid
        bookList['staff']=self.bauth
        bookList['department']=self.bdept
        bkList.append(bookList)
    def Display():
        for item in bkList:
            print(item)
            print("\n")
    def Delete(obj):
        for i in range(len(bkList)):
            if bkList[i]['id']==obj:
                del bkList[i]
                break
    def Search(obj):
        for i in range(len(bkList)):
            if bkList[i]['id']==obj:
                print("Staff member found")
                print(bkList[i])
                break
    def Update(id):
        for i in range(len(bkList)):
            if (bkList[i]['id']==id):
                bkList[i]['department']=input("enter updated department: ")
                bkList[i]['reader']=input("Enter the updated staff member: ")
                print("\nUpdate of staff member done successfully\n")
                break


class Department:
    def __init__(self,id,dept):
        self.bid=id
        self.bdept=dept
    def Add(self):
        bookList={}
        bookList['id']=self.bid
        bookList['department']=self.bdept
        bkList.append(bookList)
    def Display():
        for item in bkList:
            print(item)
            print("\n")
    def Delete(obj):
        for i in range(len(bkList)):
            if bkList[i]['id']==obj:
                del bkList[i]
                break
    def Search(obj):
        for i in range(len(bkList)):
            if bkList[i]['id']==obj:
                print("Department found")
                print(bkList[i])
                break
    def Update(id):
        for i in range(len(bkList)):
            if (bkList[i]['id']==id):
                bkList[i]['department']=input("enter updated department: ")
                print("\nUpdate of Department done successfully\n")
                break
    
        



print("\n\n=====================SIMPLE LIBRARY SYSTEM===================\n\n")
print("Library Menu\n1:Book\n2:Author\n3:LibraryStaff\n4:Readers\n5:Department\n6:Exit\n\n")
choice=int(input("Enter the choice from menu u want to operate on: "))
i=1
while(i>0):
    if (choice == 1):
        print("\n\n========Book Menu========\n\n")
        print("1:Add\n2:Delete\n3:Update\n4:Search\n5:Display\n6:Exit\n\n")
        ch = int(input("Enter the operation to perform: "))
        if ch == 1:
            name = input("enter book name: ")
            id = int(input("enter book id: "))
            author = input("enter book author name: ")
            dept = input("enter book department name: ")
            b1 = Book(name,id, author, dept)
            b1.Add()
            print("book added successfully")
            print("\n\n")
        elif ch == 2:
            Book.Delete(int(input("Enter id of the book to delete: ")))
            print("\n\n")
        elif ch == 3:
            Book.Update(int(input("enter id u want to update: ")))
            print("\n\n")
        elif ch == 4:
            Book.Search(int(input("enter book id to search: ")))
            print("\n\n")
        elif ch == 5:
            Book.Display()
            print("\n\n")
        elif ch == 6:
            exit()
    elif (choice == 2):
        print("\n\n========Author Menu========\n\n")
        print("1:Add\n2:Delete\n3:Update\n4:Search\n5:Display\n6:Exit\n\n")
        ch = int(input("Enter the operation to perform: "))
        if ch == 1:
            id = int(input("enter author id: "))
            author = input("enter author name: ")
            dept = input("enter department name: ")
            b1 = Author(id, author, dept)
            b1.Add()
            print("author added successfully")
            print("\n\n")
        elif ch == 2:
            Author.Delete(int(input("Enter id of the author to delete: ")))
            print("\n\n")
        elif ch == 3:
            Author.Update(int(input("enter id of the author u want to update: ")))
            print("\n\n")
        elif ch == 4:
            Author.Search(int(input("enter name of author id to search: ")))
            print("\n\n")
        elif ch == 5:
            Author.Display()
            print("\n\n")
        elif ch == 6:
            exit()
    elif (choice == 3):
        print("\n\n========Library staff Menu========\n\n")
        print("1:Add\n2:Delete\n3:Update\n4:Search\n5:Display\n6:Exit\n\n")
        ch = int(input("Enter the operation to perform: "))
        if ch == 1:
            id = int(input("enter Staff id: "))
            staff = input("enter staff name: ")
            dept = input("enter department name: ")
            b1 = LibraryStaff(id, staff, dept)
            b1.Add()
            print("staff added successfully")
            print("\n\n")
        elif ch == 2:
            LibraryStaff.Delete(int(input("Enter id of the staff to delete: ")))
            print("\n\n")
        elif ch == 3:
            LibraryStaff.Update(int(input("enter id of the staff u want to update: ")))
            print("\n\n")
        elif ch == 4:
            LibraryStaff.Search(int(input("enter name of staff id to search: ")))
            print("\n\n")
        elif ch == 5:
            LibraryStaff.Display()
            print("\n\n")
        elif ch == 6:
            exit()
    elif (choice == 4):
        print("\n\n========Reader Menu========\n\n")
        print("1:Add\n2:Delete\n3:Update\n4:Search\n5:Display\n6:Exit\n\n")
        ch = int(input("Enter the operation to perform: "))
        if ch == 1:
            id = int(input("enter Reader id: "))
            reader = input("enter reader name: ")
            dept = input("enter department name: ")
            b1 = Reader(id, reader, dept)
            b1.Add()
            print("Reader added successfully")
            print("\n\n")
        elif ch == 2:
            Reader.Delete(int(input("Enter id of the reader to delete: ")))
            print("\n\n")
        elif ch == 3:
            Reader.Update(int(input("enter id of the reader u want to update: ")))
            print("\n\n")
        elif ch == 4:
            Reader.Search(int(input("enter name of reader/id to search: ")))
            print("\n\n")
        elif ch == 5:
            Reader.Display()
            print("\n\n")
        elif ch == 6:
            exit()
    elif (choice == 5):
        print("\n\n========Department Menu========\n\n")
        print("1:Add\n2:Delete\n3:Update\n4:Search\n5:Display\n6:Exit\n\n")
        ch = int(input("Enter the operation to perform: "))
        if ch == 1:
            id = int(input("enter Department id: "))
            dept = input("enter department name: ")
            b1 = Department(id,dept)
            b1.Add()
            print("Department added successfully")
            print("\n\n")
        elif ch == 2:
            Department.Delete(int(input("Enter id of the department to delete: ")))
            print("\n\n")
        elif ch == 3:
            Department.Update(int(input("enter id of the department u want to update: ")))
            print("\n\n")
        elif ch == 4:
            Department.Search(int(input("enter name of department/id to search: ")))
            print("\n\n")
        elif ch == 5:
            Department.Display()
            print("\n\n")
        elif ch == 6:
            exit()
