﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShortestPath
{
    class RoutingPath
    {
        int[,] graph = new int[200, 200];
        int[] distance = new int[20];
        int[] visited = new int[20];
        int[] q = new int[20];
        int[] parent = new int[20];
        int[] vertices = new int[20];
        int final;
        int minimumDistance;
        void PrintPath(int V, int d)
        {
            int i, k = 0;
            int[] a = new int[d];
            a[k++] = final;
            while (parent[d] != -1)
            {
                a[k++] = vertices[parent[d]];
                d = parent[d];
            }
            Console.Write("The Shortest Path is ");
            for (i = k - 1; i >= 0; i--)
            {
                Console.Write(a[i] + "->");
            }
            Console.WriteLine("with a distance of " + minimumDistance);

        }
        int Queue(int V)
        {
            int sum = 0, i;
            for (i = 0; i < V; i++)
            {
                sum += q[i];
            }
            return sum;
        }
        int MinimumNode(int V)
        {
            int i, min = 1000, id = 0;
            for (i = 0; i < V; i++)
            {
                if (distance[i] <= min && visited[i] == 0)
                {
                    min = distance[i];
                    id = i;
                }

            }
            q[id] = 0;
            return id;
        }
        void FindingRoute(int S, int V, int d)
        {
            int u, i, check_empty = Queue(V);

            while (check_empty > 0)
            {
                u = MinimumNode(V);
                visited[u] = 1;
                q[u] = 0;
                for (i = 0; i < V; i++)
                {
                    if (graph[u, i] > 0)
                    {
                        if (distance[u] + graph[u, i] < distance[i])
                        {
                            distance[i] = distance[u] + graph[u, i];
                            parent[i] = u;
                            if (i == d)
                            {
                                minimumDistance = distance[i];
                            }
                        }
                    }
                }
                check_empty = Queue(V);
    
            }

            print(V, d);

        }
        public static void Main(string[] args)
        {
            int numberOfVertices, i, j, destination, source, low, high, mid;
            Routing obj = new Routing();
            Console.WriteLine("Enter no. of vertices: ");
            numberOfVertices = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the vertices ");
            for (i = 0; i < numberOfVertices; i++)
            {
                obj.vertices[i] = Int32.Parse(Console.ReadLine());
            }
            Console.WriteLine("Enter graph in matrix form:");
            for (i = 0; i < numberOfVertices; i++)
            {
                String[] input = Console.ReadLine().Split(' ');
                for (j = 0; j < numberOfVertices; j++)
                {

                    obj.graph[i, j] = Int32.Parse(input[j]);
                }

            }
            for (i = 0; i < numberOfVertices; i++)
            {
                obj.distance[i] = 1000;
                obj.visited[i] = 0;
                obj.q[i] = 1;
                obj.parent[i] = -1;
            }
            
            Console.WriteLine("Enter the source vertex: ");
            source = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter the destination vertex: ");
            destination = Int32.Parse(Console.ReadLine());
            obj.final = destination;
            obj.distance[source - 1] = 0;
            low = 0;
            high = numberOfVertices - 1;
            while (low <= high)
            {
                mid = (low + high) / 2;
                if (destination < obj.vertices[mid])
                    high = mid - 1;
                else if (destination > obj.vertices[mid])
                    low = mid + 1;
                else
                {
                    destination = mid;
                    break;
                }
            }
            obj.FindingRoute(source, numberOfVertices, destination);
        }
    }
}
