﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace AddTwoNumbers
{
    class AddBigNumbers
    {
        static void Main(string[] args)
        {
            BigInteger number1;
            BigInteger number2;
            BigInteger result;
            Console.WriteLine("Enter first number:");
            number1 = BigInteger.Parse(Console.ReadLine());
            Console.WriteLine("Enter second number:");
            number2 = BigInteger.Parse(Console.ReadLine());
            result = number1 + number2;
            Console.WriteLine(result);

        }
    }
}
