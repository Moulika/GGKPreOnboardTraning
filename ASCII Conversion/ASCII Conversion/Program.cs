﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASCII_Conversion
{
        class Conversion
        {
            public int Prime(int average)
            {
                int count = 0;
                for (int j = 2; j < average; j++)
                {
                    if (average % j == 0)
                    {
                        count++;
                    }
                    if (count >= 1)
                    {
                        break;
                    }
                }
                if (count == 0)
                    return average + 1;
                else
                    return average;
            }
            static void Main(string[] args)
            {
                Conversion obj = new Conversion();
                String s = String.Empty;
                int average;
                s = Console.ReadLine(); // input string
                byte[] b = Encoding.ASCII.GetBytes(s);
                int size = b.Length - 1;
                char[] convertedString = new char[size];
                for (int i = 0; i < size; i++)
                {
                    average = (b[i] + b[i + 1]) / 2;
                    int number = obj.Prime(average);
                    convertedString[i] = (char)number; // output string

                }
                Console.WriteLine(convertedString);
            }

        }
}
